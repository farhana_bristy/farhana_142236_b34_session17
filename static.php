<?php

    class Message
    {
        public $myNormVariable='Hello';
        public static $myStaticVariable='World';

        public function hello()
        {
            echo 'Hello world';
        }

        public static function msg($message2prompt)
        {
            echo $message2prompt;
        }
    }

    echo Message::msg("Data");