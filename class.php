<?php
    class StudentInfo
    {
        public $std_id='Hello';
        public $std_name='rupa';
        public $std_cgpa;
        public function set_std_id($std_id)
        {
            $this->std_id = $std_id;
        }

        public function get_std_id()
        {
            return $this->std_id;
        }

        public function set_std_name($std_name)
        {
            $this->std_name = $std_name;
        }

        public function set_std_cgpa($std_cgpa)
        {
            $this->std_cgpa = $std_cgpa;
        }
        public function get_std_cgpa()
        {
            return $this->std_cgpa;
        }
    }

    $obj=new StudentInfo();
    echo $obj->std_id;
    $obj->set_std_id("World");
    $obj->set_std_id('SEIP142236');
    echo '<br>';
    echo $obj->std_id;

    $result = $obj->set_std_cgpa('4.56');
    echo $result;

?>